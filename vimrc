" Custom .vimrc file by @iandin

" Setup Pathogen to manage your plugins
" mkdir -p ~/.vim/autoload ~/.vim/bundle && cd ~/.vim/autoload
" wget -O pathogen.vim http://www.vim.org/scripts/download_script.php?src_id=19375
call pathogen#runtime_append_all_bundles()
call pathogen#helptags()


" Enable syntax highlighting
filetype off
filetype plugin indent on
syntax on


" Automatic reloading of .vimrc
autocmd! bufwritepost .vimrc source %


" Better copy & paste
set pastetoggle=<F2>
set clipboard=unnamed


" Mouse and backspace
set mouse=a
set bs=2       "make backspace behave normal again


" Rebind <leader> key
let mapleader = ","


" Bind nohl
" Removes highlight of your last search
" noremap <c-n> :nohl<CR>
" vnoremap <c-n> :nohl<CR>
" inoremap <c-n> :nohl<CR>


" Easier moving between tabs
map <leader>n <esc>:tabprevious<CR>
map <leader>m <esc>:tabnext<CR>


" Bind Ctrl+<movement> keys to move around items instead Ctrl+w+<movement>
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h


" Map sort function to a key
vnoremap <leader>s :sort<CR>


" Indentantion of code blocks keeping selection
vnoremap < <gv
vnoremap > >gv


" Making trailing whitespaces visible
set list listchars=tab:>-,trail:.,extends:>


" Color scheme
" mkdir -p ~/.vim/colors && cd ~/.vim/colors
" wget -O wombat256mod.vim http://www.vim.org/scripts/download_script.php?src_id=13400
set t_Co=256
colorscheme wombat256mod


" Showing line numbers and length
set number     "show line numbers
set tw=79      "width of document (used by gd)
set nowrap     "don't automatically wrap on load
set fo=t       "dont't automatically wrap text when typing
set colorcolumn=80
highlight ColorColumn ctermbg=233


" Easier formatting of paragraphs
vmap Q gq
nmap Q gqap


" Useful settings
set history=700
set undolevels=700


" For python use spaces instead of TABs
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab


" Make search case insensitive
set hlsearch
set incsearch
set ignorecase
set smartcase



" Code folding (from plugin)
set foldmethod=indent
set foldlevel=99


" Task list plugin
map <leader>td <Plug>TaskList


" Gundo plugin
nnoremap <leader>g :GundoToggle<CR>


" Pyflakes plugin
let g:pyflakes_use_quickfix=0


" Pep8 plugin
let g:pep8_map='<leader>8'


" SuperTab Completion & Documentation plugin
au Filetype python set omnifunc=pythoncomplete#Complete
let g:SuperTabDefaultCompletionType="context"
set completeopt=menuone,longest,preview


" File browser plugin
map <leader>tr :NERDTreeToggle<CR>


" Ropevim plugin
map <leader>j :RopeGotoDefinition<CR>
map <leader>r :RopeRen ame<CR>


" ACK plugin
nmap <leader>a <Esc>:Ack!


" Git integration plugins (git.vim and fugitive)
set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P

" Test integration plugins
" Django Nose
map <leader>dt :set makeprg=python\ manage.py\ test\|:call MakeGreen()<CR>
" py.test: Execute tests
nmap <silent><leader>tf <Esc>:Pytest file<CR>
nmap <silent><leader>tc <Esc>:Pytest class<CR>
nmap <silent><leader>tm <Esc>:Pytest method<CR>
" py.test: Cycle through errors
nmap <silent><leader>tn <Esc>:Pytest next<CR>
nmap <silent><leader>tp <Esc>:Pytest previous<CR>
nmap <silent><leader>te <Esc>:Pytest error<CR>

" Add the virtualenv's site-packages to vim path
py << EOF
import os.path
import sys
import vim
if 'VIRTUAL_ENV' in os.environ:
    project_base_dir = os.environ['VIRTUAL_ENV']
    sys.path.insert(0, project_base_dir)
    activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
    execfile(activate_this, dict(__file__=activate_this))
EOF
